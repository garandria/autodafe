# Identification
libgif_VERSION_MAJOR = 5
# Compilation flags
CC = gcc
CFLAGS = -g -O2 -Wall
CPPFLAGS =
LDFLAGS =
LIBS =
# Platform
OBJEXT = o
# Cross-build
srcdir = .
# # Installation directories
prefix = /usr/local
exec_prefix = ${prefix}
includedir = ${prefix}/include
libdir = ${exec_prefix}/lib
# Installation hooks
NORMAL_INSTALL = :
NORMAL_UNINSTALL = :
# Location
top_builddir = ..
top_srcdir = ..
subdir = lib
# Internals (not configuration variables)
SHELL = /bin/sh
DIST_COMMON = $(include_HEADERS)
DISTFILES = $(DIST_COMMON) $(DIST_SOURCES) $(EXTRA_DIST)
SOURCES = $(libgetarg_a_SOURCES) $(libgif_la_SOURCES)
DIST_SOURCES = $(libgetarg_a_SOURCES) $(libgif_la_SOURCES)
LIBTOOL = libtool # $(SHELL) $(top_builddir)/libtool
CYGPATH_W = echo
DEPDIR = .deps
LIBS =
# conftrol variables end here

LIBRARIES = $(noinst_LIBRARIES)
ARFLAGS = cru
libgetarg_a_AR = ar $(ARFLAGS)
libgetarg_a_LIBADD =
am_libgetarg_a_OBJECTS = getarg.$(OBJEXT)
libgetarg_a_OBJECTS = $(am_libgetarg_a_OBJECTS)
am__strip_dir = `echo $$p | sed -e 's|^.*/||'`;
libLTLIBRARIES_INSTALL = install -c
LTLIBRARIES = $(lib_LTLIBRARIES)
libgif_la_DEPENDENCIES =
am_libgif_la_OBJECTS = dev2gif.lo dgif_lib.lo egif_lib.lo gif_err.lo \
	gif_font.lo gif_hash.lo gifalloc.lo qprintf.lo quantize.lo
libgif_la_OBJECTS = $(am_libgif_la_OBJECTS)
libgif_la_LINK = $(LIBTOOL) --tag=CC \
	$(LIBTOOLFLAGS) --mode=link $(CC) $(CFLAGS) \
	$(libgif_la_LDFLAGS) $(LDFLAGS) -o $@
DEFAULT_INCLUDES = -I. -I$(top_builddir)
COMPILE = $(CC) $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) \
	$(CPPFLAGS) $(CFLAGS)
LTCOMPILE = $(LIBTOOL) --tag=CC $(LIBTOOLFLAGS) \
	--mode=compile $(CC) $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) \
	 $(CPPFLAGS) $(CFLAGS)
includeHEADERS_INSTALL = install -c -m 644
HEADERS = $(include_HEADERS)
EXTRA_DIST = Makefile.unx
lib_LTLIBRARIES = libgif.la
noinst_LIBRARIES = libgetarg.a
include_HEADERS = gif_lib.h
libgetarg_a_SOURCES = getarg.c getarg.h
libgif_la_SOURCES = dev2gif.c   \
                    dgif_lib.c  \
                    egif_lib.c  \
                    gif_err.c   \
                    gif_font.c  \
					gif_hash.c  \
					gif_hash.h	\
                    gifalloc.c  \
                    qprintf.c   \
                    quantize.c  \
					gif_lib_private.h

libgif_la_LDFLAGS = -version-info $(libgif_VERSION_MAJOR)
libgif_la_LIBADD =    -lSM -lICE   -lX11  -lSM -lICE   -lX11
all: all-am

.SUFFIXES: .c .lo .o .obj

clean-noinstLIBRARIES:
	-test -z "$(noinst_LIBRARIES)" || rm -f $(noinst_LIBRARIES)
libgetarg.a: $(libgetarg_a_OBJECTS) $(libgetarg_a_DEPENDENCIES)
	-rm -f libgetarg.a
	$(libgetarg_a_AR) libgetarg.a $(libgetarg_a_OBJECTS) $(libgetarg_a_LIBADD)
	ranlib libgetarg.a
install-libLTLIBRARIES: $(lib_LTLIBRARIES)
	@$(NORMAL_INSTALL)
	test -z "$(libdir)" || mkdir -p "$(DESTDIR)$(libdir)"
	@list='$(lib_LTLIBRARIES)'; for p in $$list; do \
	  if test -f $$p; then \
	    f=$(am__strip_dir) \
	    echo " $(LIBTOOL) --mode=install $(libLTLIBRARIES_INSTALL) $(INSTALL_STRIP_FLAG) '$$p' '$(DESTDIR)$(libdir)/$$f'"; \
	    $(LIBTOOL) --mode=install $(libLTLIBRARIES_INSTALL) $(INSTALL_STRIP_FLAG) "$$p" "$(DESTDIR)$(libdir)/$$f"; \
	  else :; fi; \
	done

uninstall-libLTLIBRARIES:
	@$(NORMAL_UNINSTALL)
	@list='$(lib_LTLIBRARIES)'; for p in $$list; do \
	  p=$(am__strip_dir) \
	  echo " $(LIBTOOL) --mode=uninstall rm -f '$(DESTDIR)$(libdir)/$$p'"; \
	  $(LIBTOOL) --mode=uninstall rm -f "$(DESTDIR)$(libdir)/$$p"; \
	done

clean-libLTLIBRARIES:
	-test -z "$(lib_LTLIBRARIES)" || rm -f $(lib_LTLIBRARIES)
	@list='$(lib_LTLIBRARIES)'; for p in $$list; do \
	  dir="`echo $$p | sed -e 's|/[^/]*$$||'`"; \
	  test "$$dir" != "$$p" || dir=.; \
	  echo "rm -f \"$${dir}/so_locations\""; \
	  rm -f "$${dir}/so_locations"; \
	done
libgif.la: $(libgif_la_OBJECTS) $(libgif_la_DEPENDENCIES)
	$(libgif_la_LINK) -rpath $(libdir) $(libgif_la_OBJECTS) $(libgif_la_LIBADD) $(LIBS)

mostlyclean-compile:
	-rm -f *.$(OBJEXT)

distclean-compile:
	-rm -f *.tab.c

include ./$(DEPDIR)/dev2gif.Plo
include ./$(DEPDIR)/dgif_lib.Plo
include ./$(DEPDIR)/egif_lib.Plo
include ./$(DEPDIR)/getarg.Po
include ./$(DEPDIR)/gif_err.Plo
include ./$(DEPDIR)/gif_font.Plo
include ./$(DEPDIR)/gif_hash.Plo
include ./$(DEPDIR)/gifalloc.Plo
include ./$(DEPDIR)/qprintf.Plo
include ./$(DEPDIR)/quantize.Plo

.c.$(OBJEXT):
	$(COMPILE) -c -o $@ $<

.c.obj:
	$(COMPILE) -c -o $@ `$(CYGPATH_W) '$<'`
#	source='$<' object='$@' libtool=no \
#	$(COMPILE) -c `$(CYGPATH_W) '$<'`

.c.lo:
	$(LTCOMPILE) -MT $@ -MD -MP -MF $(DEPDIR)/$*.Tpo -c -o $@ $<
#	source='$<' object='$@' libtool=yes \
#	$(LTCOMPILE) -c -o $@ $<

mostlyclean-libtool:
	-rm -f *.lo

clean-libtool:
	-rm -rf .libs _libs
install-includeHEADERS: $(include_HEADERS)
	@$(NORMAL_INSTALL)
	test -z "$(includedir)" || mkdir -p "$(DESTDIR)$(includedir)"
	@list='$(include_HEADERS)'; for p in $$list; do \
	  if test -f "$$p"; then d=; else d="$(srcdir)/"; fi; \
	  f=$(am__strip_dir) \
	  echo " $(includeHEADERS_INSTALL) '$$d$$p' '$(DESTDIR)$(includedir)/$$f'"; \
	  $(includeHEADERS_INSTALL) "$$d$$p" "$(DESTDIR)$(includedir)/$$f"; \
	done

uninstall-includeHEADERS:
	@$(NORMAL_UNINSTALL)
	@list='$(include_HEADERS)'; for p in $$list; do \
	  f=$(am__strip_dir) \
	  echo " rm -f '$(DESTDIR)$(includedir)/$$f'"; \
	  rm -f "$(DESTDIR)$(includedir)/$$f"; \
	done

tags: TAGS

TAGS:  $(HEADERS) $(SOURCES)  $(TAGS_DEPENDENCIES) \
		$(TAGS_FILES)
	tags=; \
	here=`pwd`; \
	list='$(SOURCES) $(HEADERS)  $(TAGS_FILES)'; \
	unique=`for i in $$list; do \
	    if test -f "$$i"; then echo $$i; else echo $(srcdir)/$$i; fi; \
	  done | \
	  awk '    { files[$$0] = 1; } \
	       END { for (i in files) print i; }'`; \
	if test -z "$(ETAGS_ARGS)$$tags$$unique"; then :; else \
	  test -n "$$unique" || unique=$$empty_fix; \
	  etags $(ETAGSFLAGS) $(ETAGS_ARGS) \
	    $$tags $$unique; \
	fi
ctags: CTAGS
CTAGS:  $(HEADERS) $(SOURCES)  $(TAGS_DEPENDENCIES) \
		$(TAGS_FILES)
	tags=; \
	here=`pwd`; \
	list='$(SOURCES) $(HEADERS)  $(TAGS_FILES)'; \
	unique=`for i in $$list; do \
	    if test -f "$$i"; then echo $$i; else echo $(srcdir)/$$i; fi; \
	  done | \
	  awk '    { files[$$0] = 1; } \
	       END { for (i in files) print i; }'`; \
	test -z "$(CTAGS_ARGS)$$tags$$unique" \
	  || ctags $(CTAGSFLAGS) $(CTAGS_ARGS) \
	     $$tags $$unique

GTAGS:
	here=`cd >/dev/null $(top_builddir) && pwd` \
	  && cd $(top_srcdir) \
	  && gtags -i $(GTAGS_ARGS) $$here

distclean-tags:
	-rm -f TAGS GTAGS GRTAGS GSYMS GPATH tags

distdir: $(DISTFILES)
	@srcdirstrip=`echo "$(srcdir)" | sed 's/[].[^$$\\*]/\\\\&/g'`; \
	topsrcdirstrip=`echo "$(top_srcdir)" | sed 's/[].[^$$\\*]/\\\\&/g'`; \
	list='$(DISTFILES)'; \
	  dist_files=`for file in $$list; do echo $$file; done | \
	  sed -e "s|^$$srcdirstrip/||;t" \
	      -e "s|^$$topsrcdirstrip/|$(top_builddir)/|;t"`; \
	case $$dist_files in \
	  */*) mkdir -p `echo "$$dist_files" | \
			   sed '/\//!d;s|^|$(distdir)/|;s,/[^/]*$$,,' | \
			   sort -u` ;; \
	esac; \
	for file in $$dist_files; do \
	  if test -f $$file || test -d $$file; then d=.; else d=$(srcdir); fi; \
	  if test -d $$d/$$file; then \
	    dir=`echo "/$$file" | sed -e 's,/[^/]*$$,,'`; \
	    if test -d $(srcdir)/$$file && test $$d != $(srcdir); then \
	      cp -pR $(srcdir)/$$file $(distdir)$$dir || exit 1; \
	    fi; \
	    cp -pR $$d/$$file $(distdir)$$dir || exit 1; \
	  else \
	    test -f $(distdir)/$$file \
	    || cp -p $$d/$$file $(distdir)/$$file \
	    || exit 1; \
	  fi; \
	done
check-am: all-am
check: check-am
all-am: $(LIBRARIES) $(LTLIBRARIES) $(HEADERS)
installdirs:
	for dir in "$(DESTDIR)$(libdir)" "$(DESTDIR)$(includedir)"; do \
	  test -z "$$dir" || mkdir -p "$$dir"; \
	done
install: install-am
install-exec: install-exec-am
install-data: install-data-am
uninstall: uninstall-am

install-am: all-am
	@$(MAKE) install-exec-am install-data-am

installcheck: installcheck-am
install-strip:
	$(MAKE) INSTALL_PROGRAM="install -c -s" \
	  install_sh_PROGRAM="install -c -s" INSTALL_STRIP_FLAG=-s \
	  `test -z 'strip' || \
	    echo "INSTALL_PROGRAM_ENV=STRIPPROG='strip'"` install
mostlyclean-generic:

clean-generic:

distclean-generic:
	-test -z "$(CONFIG_CLEAN_FILES)" || rm -f $(CONFIG_CLEAN_FILES)

clean: clean-am

clean-am: clean-generic clean-libLTLIBRARIES clean-libtool \
	clean-noinstLIBRARIES mostlyclean-am

distclean: distclean-am
	-rm -rf ./$(DEPDIR)
distclean-am: clean-am distclean-compile distclean-generic \
	distclean-tags

html: html-am

install-data-am: install-includeHEADERS

install-exec-am: install-libLTLIBRARIES

install-html: install-html-am

install-man:

installcheck-am:

mostlyclean: mostlyclean-am

mostlyclean-am: mostlyclean-compile mostlyclean-generic \
	mostlyclean-libtool

uninstall-am: uninstall-includeHEADERS uninstall-libLTLIBRARIES

.PHONY: CTAGS GTAGS all all-am check check-am clean clean-generic \
	clean-libLTLIBRARIES clean-libtool clean-noinstLIBRARIES ctags \
	distclean distclean-compile distclean-generic \
	distclean-libtool distclean-tags distdir html \
	html-am install install-am install-data \
	install-data-am install-exec \
	install-exec-am install-html install-html-am \
	install-includeHEADERS \
	install-libLTLIBRARIES install-man \
	install-strip installcheck \
	installcheck-am installdirs \
	mostlyclean mostlyclean-compile \
	mostlyclean-generic mostlyclean-libtool \
	tags uninstall uninstall-am uninstall-includeHEADERS \
	uninstall-libLTLIBRARIES

